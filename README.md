# dfile
[![pipeline status](https://gitlab.com/Danno131313/dfile-rs/badges/master/pipeline.svg)](https://gitlab.com/Danno131313/dfile-rs/commits/master)

A small Rust program for organizing your dotfiles.

Running ```dfile add <FILES>``` (accepts glob patterns) will attempt to track the FILES via Git, at your $DOTFILE_PATH directory, creating it if it doesn't exist.

Running ```dfile remove <FILES>``` (also accepts glob patterns) will stop tracking and remove the listed files from the Git repo. Input paths like ```/etc/pacman.conf``` (original file locations).

Running ```dfile list``` will display all currently tracked files.

Running ```dfile push``` will attempt to add, commit, and push all changes in the dotfile directory to a remote repo, setting one up if none exists.

Running ```dfile restore [<FILES>]``` will copy your tracked files back to their original locations. If no files are listed, it will copy all of them. Running this command without a $DOTFILE_PATH Git repo set up will prompt you to clone a repo first.

You can also run Git command from your $DOTFILE_PATH by running ```dfile git -- <SUBCOMMANDS>```.

Run ```dfile --help``` and ```dfile <SUBCOMMAND> --help``` for all this information.
