#[macro_use]
extern crate log;

use std::env::var;
use std::path::PathBuf;
use std::process::exit;

use git2::Repository;
use structopt::StructOpt;

use crate::commands::{
    git_update, list_files, process_files, remove_files, restore_files, run_git, try_clone,
};
use crate::setup::new_git;

mod commands;
mod models;
mod setup;

/// A program to easily copy dotfiles to a directory for git management and backup.
///
/// Uses your $HOME, $DOTFILE_PATH, and $PRIMARY_SSH_KEY environment variables. $DOTFILE_PATH should be a folder in your home directory where the copies will be stored, and $PRIMARY_SSH_KEY should be the path to the SSH key to use for pushing to a remote.

#[derive(StructOpt, Debug)]
#[structopt(name = "dfile", usage = "dfile [OPTIONS] [SUBCOMMAND] [--help]")]
pub struct Opt {
    #[structopt(
        long,
        short,
        help = "debug, trace, info, warn, error (default), or none."
    )]
    pub verbosity: Option<String>,
    #[structopt(subcommand)]
    pub cmd: Command,
}

#[derive(StructOpt, Debug, PartialEq)]
pub enum Command {
    #[structopt(about = "Adds files to the Git repo at $DOTFILE_PATH and tracks them.")]
    Add {
        #[structopt(
            help = "File glob patterns, seperated by spaces, to add to the DOTFILE_PATH Git repo."
        )]
        files: Vec<String>,
    },
    #[structopt(
        about = "Removes files from the Git repo and stops tracking them (use the file's original path)."
    )]
    Remove {
        #[structopt(
            help = "File glob patterns, seperated by spaces, to remove from the DOTFILE_PATH Git repo."
        )]
        files: Vec<String>,
    },
    #[structopt(about = "Lists currently tracked files.")]
    List,
    #[structopt(
        about = "Restores files from your $DOTFILE_PATH repo to their original locations."
    )]
    Restore {
        #[structopt(
            help = "File glob patterns, seperated by spaces, to restore to their original locations from the DOTFILE_PATH Git repo. Use the original paths.\n\nIf no path is inputted, all tracked files are restored to their original locations."
        )]
        files: Vec<String>,
    },
    #[structopt(
        about = "Copies all tracked files from their original locations to $DOTFILE_PATH, then commits and pushes them to a remote."
    )]
    Push,
    #[structopt(about = "Runs Git commands from inside the $DOTFILE_PATH repo.")]
    Git {
        #[structopt(
            help = "Subcommands to pass to Git.",
            allow_hyphen_values = true,
            last = true
        )]
        commands: Vec<String>,
    },
}

fn main() {
    let opt: Opt = Opt::from_args();
    let verbosity = opt.verbosity;

    if let Some(v) = verbosity {
        let v = match v.to_lowercase().as_str() {
            "debug" | "d" => "debug",
            "info" | "i" => "info",
            "trace" | "t" => "trace",
            "warn" | "w" => "warn",
            "error" | "e" => "error",
            "none" | "" | "n" => "",
            _ => "info",
        };
        if v != "" {
            std::env::set_var("RUST_LOG", &format!("dfile={}", v));
        }
    } else {
        std::env::set_var("RUST_LOG", "dfile=error");
    }
    pretty_env_logger::init();

    let dotfile_path = match var("DOTFILE_PATH") {
        Ok(path) => path,
        Err(_) => {
            error!("You need to set the DOTFILE_PATH environment variable to use this program. Exiting...");
            exit(1);
        }
    };

    match opt.cmd {
        Command::Restore { files } => {
            match Repository::open(&dotfile_path) {
                Ok(_) => restore_files(files),
                Err(_) => {
                    try_clone(&PathBuf::from(&dotfile_path)).unwrap();
                    exit(0);
                }
            };
        }
        Command::Add { files } => {
            debug!("{:?}", files);
            match Repository::open(dotfile_path) {
                Ok(r) => process_files(files, r).unwrap(),
                Err(_) => {
                    let r = new_git();
                    process_files(files, r).unwrap();
                }
            };
        }
        Command::Remove { files } => {
            match Repository::open(dotfile_path) {
                Ok(r) => remove_files(files, r).unwrap(),
                Err(_) => {
                    error!("No Git repo set up, nothing to remove.");
                    exit(1);
                }
            };
        }
        Command::List => {
            match Repository::open(&dotfile_path) {
                Ok(_) => list_files().unwrap(),
                Err(_) => {
                    try_clone(&PathBuf::from(&dotfile_path)).unwrap();
                    exit(0);
                }
            };
        }
        Command::Push => {
            match Repository::open(&dotfile_path) {
                Ok(r) => {
                    let result = git_update(&r);
                    match result {
                        Ok(()) => {
                            info!("Successfully updated dotfile git repo.");
                            exit(0);
                        }

                        Err(e) => {
                            error!("Error updating dotfile git repo: {}", e);
                            exit(1);
                        }
                    }
                }
                Err(_) => new_git(),
            };
        }
        Command::Git { commands } => {
            debug!("{:?}", commands);
            run_git(commands, &dotfile_path).unwrap();
        }
    }
}
