use std::env::var;
use std::io;
use std::io::Write;
use std::process::exit;

use git2::Repository;

/// Creates a new git repo at the DOTFILE_PATH directory
pub fn new_git() -> Repository {
    let input = prompt("No git repo found at DOTFILE_PATH, would you like to create one? (y/n) ");
    if input.trim() == "y" {
        Repository::init(var("DOTFILE_PATH").unwrap())
            .expect("Couldn't create a new git repo with DOTFILE_PATH")
    } else {
        exit(0);
    }
}

pub fn prompt(s: &str) -> String {
    print!("{}", s);
    io::stdout().flush().unwrap();
    let mut input = String::new();
    io::stdin().read_line(&mut input).unwrap();
    input.pop();

    input
}

pub fn setup_remote(repo: &Repository) {
    let response =
        prompt("You haven't set up a remote for your dotfile repo yet, would you like to? (y/n) ");

    if var("PRIMARY_SSH_KEY").is_err() {
        error!("You must first set the PRIMARY_SSH_KEY environment variable to use remotes (full path to your SSH key to use with Git). Exiting...");
        exit(1);
    }

    if response.trim() == "n" {
        exit(0);
    } else {
        let remote = prompt("What is the SSH address of the git remote repo?: ");
        repo.remote_set_pushurl("origin", Some(remote.trim()))
            .expect("Couldn't set remote push url");
        repo.remote_set_url("origin", remote.trim())
            .expect("Couldn't set remote push url");
    }
}
