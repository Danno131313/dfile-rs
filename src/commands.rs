use std::collections::HashSet;
use std::env::{current_dir, var};
use std::error::Error;
use std::fs::create_dir_all;
use std::path::{Path, PathBuf};
use std::process::Command;

use dirs::home_dir;
use git2::*;
use glob::glob;
use time::PrimitiveDateTime;

use super::setup::{prompt, setup_remote};
use crate::models::*;

/// Add files to the dotfile repo
pub fn process_files(files: Vec<String>, repo: Repository) -> Result<(), Box<dyn Error>> {
    let dotfile_path: PathBuf = var("DOTFILE_PATH").unwrap().into();
    let curr_dir: PathBuf = current_dir().unwrap();
    let mut filelist_path = dotfile_path;
    filelist_path.push("filelist.yml");

    let mut yml: FileList = if filelist_path.exists() {
        let filelist_file = std::fs::File::open(&filelist_path)?;
        let yml: FileList = serde_yaml::from_reader(filelist_file)?;
        yml
    } else {
        FileList { files: Vec::new() }
    };

    let mut filelist: HashSet<TrackedFile> = yml.files.clone().into_iter().collect();
    let mut new_files: Vec<TrackedFile> = Vec::new();

    let mut total = 0;
    for maybe_glob in files.into_iter() {
        let globbed = glob(&maybe_glob).unwrap();

        for maybe_file in globbed {
            let file = maybe_file.unwrap();
            let name = file.to_str().unwrap();

            let fullpath = curr_dir.join(&file).canonicalize().unwrap();
            if !fullpath.exists() {
                error!("{} does not exist", fullpath.to_str().unwrap());
                continue;
            }

            let tracked_file = TrackedFile::new(&fullpath);

            // Create new directories in dotfile path if they don't exist
            let newpath_clone = tracked_file.dest();
            let dirs_only = newpath_clone.parent().unwrap();
            create_dir_all(dirs_only)?;

            std::fs::copy(&tracked_file.source(), &tracked_file.dest())?;

            filelist.insert(tracked_file.clone());
            new_files.push(tracked_file);

            total += 1;

            info!(
                "{} has been successfully copied to dotfiles directory.",
                name
            );
        }
    }

    if total == 0 {
        error!("No files matched, nothing added.");
        std::process::exit(1);
    }

    let filelist: Vec<TrackedFile> = filelist.into_iter().collect();

    let mut new_files: Vec<String> = new_files.into_iter().map(|f| f.source_string()).collect();
    new_files.insert(0, "Add all files:".to_owned());

    yml.files = filelist;
    yml.files.sort();
    let filelist_file = std::fs::File::create(filelist_path)?;
    serde_yaml::to_writer(filelist_file, &yml)?;

    let commit_msg = new_files.join("\n");

    add_commit_all(&repo, &commit_msg)?;

    Ok(())
}

pub fn remove_files(files: Vec<String>, repo: Repository) -> Result<(), Box<dyn Error>> {
    let dotfile_path: PathBuf = var("DOTFILE_PATH").unwrap().into();
    let curr_dir: PathBuf = current_dir().unwrap();

    let mut filelist_path = dotfile_path;
    filelist_path.push("filelist.yml");

    let mut yml: FileList = if filelist_path.exists() {
        let filelist_file = std::fs::File::open(&filelist_path)?;
        let yml: FileList = serde_yaml::from_reader(filelist_file)?;
        yml
    } else {
        error!("No file list found, nothing to remove.");
        std::process::exit(1);
    };

    let mut filelist: HashSet<TrackedFile> = yml.files.clone().into_iter().collect();
    let mut removed: Vec<TrackedFile> = Vec::new();

    let mut total = 0;
    for maybe_glob in files.into_iter() {
        let globbed = glob(&maybe_glob)?;

        for maybe_file in globbed {
            let file = maybe_file?;

            let fullpath = curr_dir.join(&file).canonicalize().unwrap();

            let tracked_file = TrackedFile::new(&fullpath);

            if filelist.remove(&tracked_file) {
                removed.push(tracked_file);
                total += 1;
            }
        }
    }

    if total == 0 {
        error!("No files matched, nothing removed.");
        std::process::exit(1);
    }

    let filelist: Vec<TrackedFile> = filelist.into_iter().collect();

    for tracked_file in removed.clone() {
        let mut index = repo.index()?;
        index.remove_path(&tracked_file.dest())?;

        index.write()?;

        std::fs::remove_file(&tracked_file.dest())?;

        debug!(
            "{} has been successfully removed from the dotfiles directory.",
            tracked_file.source_string()
        );
    }

    let mut removed: Vec<String> = removed.into_iter().map(|f| f.source_string()).collect();
    removed.insert(0, "Remove all files:".to_owned());

    yml.files = filelist;
    yml.files.sort();
    let filelist_file = std::fs::File::create(filelist_path)?;
    serde_yaml::to_writer(filelist_file, &yml)?;

    let commit_msg = removed.join("\n");

    add_commit_all(&repo, &commit_msg)?;

    Ok(())
}

pub fn list_files() -> Result<(), Box<dyn Error>> {
    let dotfile_path: PathBuf = var("DOTFILE_PATH").unwrap().into();

    let mut filelist_path = dotfile_path;
    filelist_path.push("filelist.yml");

    let mut yml: FileList = if filelist_path.exists() {
        let filelist_file = std::fs::File::open(&filelist_path)?;
        let yml: FileList = serde_yaml::from_reader(filelist_file)?;
        yml
    } else {
        error!("No file list found, nothing to list.");
        std::process::exit(1);
    };

    yml.files.sort();
    println!("Here are all the currently tracked files:");
    for file in yml.files {
        println!(
            "  - {}",
            file.source_string()
                .replace(home_dir().unwrap().to_str().unwrap(), "$HOME")
        );
    }

    Ok(())
}

// Adds all files in dotfile path to be committed, commits them,
// and pushes the changes to the remote repository.
pub fn git_update(repo: &Repository) -> Result<(), Box<dyn Error>> {
    if repo.head().is_err() {
        info!("Nothing to update, exiting...");
        std::process::exit(0);
    }

    info!("Copying all files and pushing to remote...");
    copy_all_files()?;
    let mut status_options = StatusOptions::new();
    status_options.include_untracked(true);
    let statuses = repo.statuses(Some(&mut status_options))?;
    if !statuses.is_empty() {
        add_commit_all(repo, &format!("update changes: {}", get_current_time()))?;
    }

    if repo.remotes()?.is_empty() {
        setup_remote(repo);
    }

    let mut rc = RemoteCallbacks::new();
    rc.credentials(move |_, username, _| {
        Cred::ssh_key(
            &username.unwrap_or("git"),
            None,
            Path::new(&var("PRIMARY_SSH_KEY").unwrap()),
            None,
        )
    });

    let mut rc2 = RemoteCallbacks::new();
    rc2.credentials(move |_, username, _| {
        Cred::ssh_key(
            &username.unwrap_or("git"),
            None,
            Path::new(&var("PRIMARY_SSH_KEY").unwrap()),
            None,
        )
    });

    let mut po = PushOptions::new();
    po.remote_callbacks(rc2);

    let mut remote = repo.find_remote("origin")?;

    debug!("Pre-auth...");
    remote.connect_auth(Direction::Push, Some(rc), None)?;

    debug!("Pre-push...");
    remote.push(&["refs/heads/master:refs/heads/master"], Some(&mut po))?;

    Ok(())
}

// If user enters 'git' as first argument, program will run the commands
// as if 'git -C dotfile_path' was called.
pub fn run_git(mut commands: Vec<String>, dotfile_path: &str) -> Result<(), Box<dyn Error>> {
    commands.insert(0, "-C".to_string());
    commands.insert(1, dotfile_path.to_string());

    debug!("Commands: {:?}", commands);
    let mut handle = Command::new("git").args(&commands).spawn()?;

    handle.wait().expect("couldnt wait for handle");

    Ok(())
}

// Gets the correct path for your dotfiles to be stored.
// Removes dots, so $HOME/.config/vimrc is copied to $HOME/dotfile_path/home_files/config/vimrc,
// for example. System files would go to somewhere like
// $HOME/dotfile_path/system_files/etc/systemd/system/something.service
pub fn get_dest(fullpath: &PathBuf, dotfile_path: &PathBuf) -> PathBuf {
    // Remove dots from provided path
    let mut dotless_newpath = PathBuf::new();

    for part in fullpath.components() {
        let part_path: &Path = part.as_ref();
        let mut part_str = part_path.to_str().unwrap();
        if part_str.starts_with('.') {
            part_str = &part_str[1..];
        }
        dotless_newpath.push(part_str);
    }

    let mut newpath_string = dotfile_path.to_str().unwrap().to_owned();

    let homedir = home_dir().unwrap();

    if fullpath.starts_with(&homedir) {
        let dir = dotless_newpath.strip_prefix(&homedir).unwrap();
        newpath_string.push_str("/home_files/");
        newpath_string.push_str(&dir.to_str().unwrap());
    } else {
        newpath_string.push_str("/system_files");
        newpath_string.push_str(&dotless_newpath.to_str().unwrap());
    }

    PathBuf::from(&newpath_string)
}

pub fn get_current_time() -> String {
    PrimitiveDateTime::now().format("%b %d, %Y (%H:%M:%S UTC)")
}

pub fn find_last_commit(repo: &Repository) -> Result<Commit, git2::Error> {
    let obj = repo.head()?.resolve()?.peel(ObjectType::Commit)?;
    obj.into_commit()
        .map_err(|_| git2::Error::from_str("Couldn't find commit"))
}

pub fn add_commit_all(repo: &Repository, message: &str) -> Result<(), Box<dyn Error>> {
    let mut index = repo.index()?;
    index.add_all(&["*"], git2::IndexAddOption::DEFAULT, Some(&mut |_, _| 0))?;

    let oid = index.write_tree()?;
    index.write()?;

    let config = Config::open_default()?;
    let username = config.get_string("user.name")?;
    let email = config.get_string("user.email")?;

    let sig = Signature::now(&username, &email)?;
    let tree = repo.find_tree(oid)?;
    let parent_commit = find_last_commit(&repo);

    if let Ok(c) = parent_commit {
        repo.commit(Some("HEAD"), &sig, &sig, message, &tree, &[&c])?;
    } else {
        repo.commit(Some("HEAD"), &sig, &sig, message, &tree, &[])?;
    }

    Ok(())
}

fn copy_all_files() -> Result<(), Box<dyn Error>> {
    let dotfile_path: PathBuf = var("DOTFILE_PATH").unwrap().into();

    let mut filelist_path = dotfile_path;
    filelist_path.push("filelist.yml");

    let yml: FileList = if filelist_path.exists() {
        let filelist_file = std::fs::File::open(&filelist_path)?;
        let yml: FileList = serde_yaml::from_reader(filelist_file)?;
        yml
    } else {
        FileList { files: Vec::new() }
    };

    let filelist: Vec<TrackedFile> = yml.files.into_iter().collect();

    for file in filelist {
        std::fs::copy(&file.source(), &file.dest())?;
    }

    Ok(())
}

pub fn restore_files(files: Vec<String>) {
    info!("Restoring files in $DOTFILE_PATH to their original locations...");
    let dotfile_path: PathBuf = var("DOTFILE_PATH").unwrap().into();

    let mut filelist_path = dotfile_path.clone();
    filelist_path.push("filelist.yml");

    let yml: FileList = if filelist_path.exists() {
        let filelist_file = std::fs::File::open(&filelist_path).unwrap();
        let yml: FileList = serde_yaml::from_reader(filelist_file).unwrap();
        yml
    } else {
        info!("No tracked file list exists! Exiting...");
        std::process::exit(0);
    };

    let mut total = 0;
    let files: Vec<String> = files
        .into_iter()
        .map(|f| {
            get_dest(&PathBuf::from(&f), &dotfile_path)
                .to_str()
                .unwrap()
                .to_owned()
        })
        .collect();

    let files = if !files.is_empty() {
        let mut selected_files = Vec::new();
        for maybe_glob in files.into_iter() {
            let globbed = glob(&maybe_glob).unwrap();

            for maybe_file in globbed {
                let file = maybe_file.unwrap();
                let path = file.to_str().unwrap().to_owned();
                selected_files.push(path);
            }
        }

        let mut tracked_files = Vec::new();
        for file in yml.files {
            for selected_file in &selected_files {
                if selected_file == &file.dest_string() {
                    tracked_files.push(file.clone())
                }
            }
        }

        tracked_files
    } else {
        yml.files
    };

    for file in files {
        total += 1;
        let res = create_dir_all(file.source().parent().unwrap());
        if res.is_err() {
            error!(
                "Cannot restore to {}, permission denied.",
                file.source_string()
            );
            continue;
        }
        match std::fs::copy(&file.dest(), &file.source()) {
            Ok(_) => (),
            Err(e) => {
                if e.kind() == std::io::ErrorKind::PermissionDenied {
                    std::process::Command::new("sudo")
                        .args(&["cp", &file.dest_string(), &file.source_string()])
                        .output()
                        .unwrap_or_else(|_| {
                            panic!(
                                "Error copying file to restricted location {}",
                                file.source_string()
                            )
                        });
                }
            }
        }
    }

    info!("Finished restoring {} file(s).", total);
    std::process::exit(0);
}

pub fn try_clone(path: &PathBuf) -> Result<(), Box<dyn Error>> {
    println!("No repository found at $DOTFILE_PATH.");
    let res = prompt("Would you like to clone a repo? (y/n): ");

    if res.trim() == "y" {
        if var("PRIMARY_SSH_KEY").is_err() {
            error!("Error, the PRIMARY_SSH_KEY environment variable must be set to a path to a remote-enabled SSH key. Exiting...");
            std::process::exit(1);
        }

        let url = prompt("What is the SSH clone url?: ");
        let mut rc = RemoteCallbacks::new();
        rc.credentials(move |_, username, _| {
            Cred::ssh_key(
                &username.unwrap_or("git"),
                None,
                Path::new(&var("PRIMARY_SSH_KEY").unwrap()),
                None,
            )
        });

        let mut fo = FetchOptions::new();
        fo.remote_callbacks(rc);

        let mut builder = git2::build::RepoBuilder::new();
        let repo = builder
            .fetch_options(fo)
            .remote_create(|r, s1, s2| {
                debug!("{}, {}", s1, s2);
                r.remote(s1, s2)
            })
            .clone(&url.trim(), path)?;

        repo.remote_set_pushurl("origin", Some(path.to_str().unwrap()))?;
        println!("Repo has been successfully cloned. Run 'dfile restore' to restore all files.");
    }

    Ok(())
}
