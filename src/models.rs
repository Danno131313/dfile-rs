use std::cmp::Ordering;
use std::env::var;
use std::path::{Path, PathBuf};

use serde_derive::{Deserialize, Serialize};

use crate::commands::*;

/// YML struct for storing list of files that have been added
#[derive(Debug, Serialize, Deserialize)]
pub struct FileList {
    pub files: Vec<TrackedFile>,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq, Hash)]
pub struct TrackedFile {
    source: PathBuf,
    dest: PathBuf,
}

fn interpolate(path: PathBuf, var_name: &str) -> PathBuf {
    let path_str = path.to_str().unwrap().to_owned();
    let var_value = var(var_name).unwrap();
    let interpolated = path_str.replace(&format!("${}", var_name), &var_value);
    let final_path = interpolated.replace("//", "/");
    PathBuf::from(final_path)
}

fn strip(path: PathBuf, var_name: &str) -> PathBuf {
    let path_str = path.to_str().unwrap().to_owned();
    let var_value = var(var_name).unwrap();
    let stripped = path_str.replace(&var_value, &format!("${}/", var_name));
    let final_path = stripped.replace("//", "/");
    PathBuf::from(final_path)
}

impl TrackedFile {
    pub fn new<P: AsRef<Path>>(path: P) -> Self {
        let path = path.as_ref().to_owned();
        let dotfile_path: PathBuf = var("DOTFILE_PATH").unwrap().into();
        let newpath: PathBuf = get_dest(&path, &dotfile_path);
        let dest = strip(newpath, "DOTFILE_PATH");
        let source = strip(path, "HOME");

        TrackedFile { source, dest }
    }

    pub fn source(&self) -> PathBuf {
        interpolate(self.source.clone(), "HOME")
    }

    pub fn dest(&self) -> PathBuf {
        interpolate(self.dest.clone(), "DOTFILE_PATH")
    }

    pub fn source_string(&self) -> String {
        let source = self.source();
        source.to_str().unwrap().to_owned()
    }

    pub fn dest_string(&self) -> String {
        let dest = self.dest();
        dest.to_str().unwrap().to_owned()
    }
}

impl Ord for TrackedFile {
    fn cmp(&self, other: &Self) -> Ordering {
        self.source.cmp(&other.source)
    }
}

impl PartialOrd for TrackedFile {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
